package user;

public class Note {

	private int ID;
	private String message;
	
	public Note(String m){
		setMessage(m);
		this.ID = IdGenerator.newID();
	}
	
	public void setMessage(String m){
		this.message = m;
	}
	
	public int getID(){
		return this.ID;
	}
	
	public String getMessage(){
		return this.message;
	}
}
