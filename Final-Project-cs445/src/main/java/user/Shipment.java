package user;

import java.time.DayOfWeek;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;

import wine.MonthlySelection;
import enums.DeliveryStatus;
import enums.MonthlySelectionType;

public class Shipment {
    private int sid;

    public int getSid(){
        return this.sid;
    }
    
    private ArrayList<Note> notes = new ArrayList<Note>();
	private String recipient = null;
	private DayOfWeek deliveryDay;
	private String TOD;
	private Subscriber sub;
	private int size;
	private MonthlySelectionType mst;
	private DeliveryStatus status;
	private int ID, UID;
	private YearMonth ym;
	private MonthlySelection ms;
	
	//Creates a Delivery for the next month
	public Shipment(Subscriber newSub, DayOfWeek dow, MonthlySelection ms, DeliveryStatus status){
		this.sub = newSub;
		this.deliveryDay = dow;
		this.mst = newSub.getPreference();
		this.size = 1;
		this.ID = IdGenerator.newID();
		this.UID = newSub.getID();
		this.ym = YearMonth.now().plus(1, ChronoUnit.MONTHS);
		this.TOD = "PM";
		this.ms = ms;
		this.status = status;
	}
	
	public Collection<Note> getNotes() {
		return notes;
	}

	public String getRecipient() {
		return recipient;
	}

	public DayOfWeek getDeliveryDay() {
		return deliveryDay;
	}

	public String getTOD() {
		return TOD;
	}

	public Subscriber getSub() {
		return sub;
	}

	public int getSize() {
		return size;
	}

	public MonthlySelectionType getMst() {
		return mst;
	}

	public DeliveryStatus getStatus() {
		return status;
	}

	public int getID() {
		return ID;
	}

	public int getUID() {
		return UID;
	}

	public YearMonth getYm() {
		return ym;
	}

	public MonthlySelection getMs() {
		return ms;
	}

	public int addNote(String m){
		Note n = new Note(m);
		notes.add(n);
		return n.getID();
	}
}
