package user;


public abstract class CustomResponse {
	boolean status;
	String feedback;
	
	public CustomResponse(boolean status, String feedback){
		this.status = status;
		this.feedback = feedback;
	}
	
	public boolean getStatus(){
		return this.status;
	}
	
	public String getFeedback(){
		return this.feedback;
	}
}
