package user;


public abstract class AddNoteRequest {
	String message;
	
	public AddNoteRequest(String m){
		this.message = m;
	}
	
	public abstract CustomResponse addNote(Shipment shipment);
}
