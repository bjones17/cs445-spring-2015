package user;


import java.io.Serializable;
import java.util.GregorianCalendar;
import java.util.Collection;
import java.util.ArrayList;

import user.IdGenerator;
import enums.AMPM;
import enums.Day;
import enums.MonthlySelectionType;
import wine.Delivery;
import wine.MonthlySelection;

public class Subscriber implements Serializable
{
    private String name, email, phone, twitter, facebook;
    private String address;
    private MonthlySelectionType mst;
    private int ID;
    private String date;
    private Collection<Shipment> shipmentCollection = new ArrayList<Shipment>();
    private Delivery delivery= new Delivery(Day.Tue, AMPM.AM);
    
    public Subscriber () 
    {
    	this.name = "Jane Doe";
    	this.email = "jane.doe@example.com";
    	this.phone = "1234567890";
    	this.address = "123 AnyStreet, Anywhere, AnyState";
    	this.mst = MonthlySelectionType.RW;
    	this.ID = IdGenerator.newID();
    	this.date = getStrDate();
    }
    
    public Subscriber (String name, String email, String phone, String address) 
    {
    	this.name = name;
    	this.email = email;
    	this.phone = phone.replaceAll("[\\s\\-()]", ""); // drop all non-digit characters
    	this.address = address;
    	this.mst = MonthlySelectionType.RW;
    	this.ID = IdGenerator.newID();
    }
    
    public Subscriber (String name, String email, String phone, String address, String fb, String tw) 
    {
    	this.name = name;
    	this.email = email;
    	this.phone = phone.replaceAll("[\\s\\-()]", ""); // drop all non-digit characters
    	this.address = address;
    	this.twitter = tw;
    	this.facebook = fb;
    	this.mst = MonthlySelectionType.RW;
    	this.ID = IdGenerator.newID();
    }

    private boolean isMatchName(String kw) 
    {
    	String regex = "(?i).*" + kw + ".*";
    	return this.name.matches(regex);
    }

    private boolean isMatchEmail(String kw) 
    {
    	String regex = "(?i).*" + kw + ".*";
    	return this.email.matches(regex);
    }

    private boolean isMatchPhone(String kw) 
    {
    	String s = kw.replaceAll("[\\s\\-()]", ""); // drop all non-digit characters from search string
    	String regex = "(?i).*" + s + ".*";
    	return this.phone.matches(regex);
    }
    public boolean isMatch(String kw) 
    {
    	if (isMatchName(kw) || isMatchEmail(kw) || isMatchPhone(kw)) {
    		return true;
    	} else return false;
    }

    public int getID()
    {
    	return this.ID;
    }
    
    public Delivery getDelivery(){
    	return this.delivery;
    }

    public void updateName(String name)
    {
    	this.name = name;
    }
    
    public void updateEmail(String email)
    {
    	this.email = email;
    }
    
    public void updatePhone(String phone)
    {
    	this.phone = phone;
    }
    
//    public void updateStreet(String street)
//    {
//    	this.address = new Address(street, address.getCity(),
//    			address.getState(), address.getZip());
//    }
//    
//    public void updateCity(String city)
//    {
//    	this.address = new Address(address.getStreet(), city,
//    			address.getState(), address.getZip());
//    }
//    
//    public void updateState(String state)
//    {
//    	this.address = new Address(address.getStreet(), address.getCity(),
//    			state, address.getZip());
//    }
//    
//    public void updateZip(String zip)
//    {
//    	this.address = new Address(address.getStreet(), address.getCity(),
//    			address.getState(), zip);
//    }
//    
//    public void updateAddress(Address address)
//    {
//    	this.address = address;
//    }
    
    public void updateTwitter(String twitter)
    {
    	this.twitter = twitter;
    }
    
    public void updateFacebook(String fb)
    {
    	this.facebook = fb;
    }
    
    public MonthlySelectionType getPreference()
    {
    	return mst;
    }

    public void setPreference(MonthlySelectionType t)
    {
    	this.mst = t;
    }
    
    public String getDateCreated()
    {
    	return date;
    }
    
    public Collection<Shipment> getShipments(){
    	return this.shipmentCollection;
    }

//    public JSONObject toJSON()
//    {
//    	JSONObject sub = new JSONObject();
//    	sub.put("uid", new Integer(ID));
//    	sub.put("date_created", getDateCreated());
//    	sub.put("type", getPreference());
//    	sub.put("name", name);
//    	sub.put("email", email);
//    	sub.put("phone", phone);
//    	sub.put("address", address.toJSON());
//    	sub.put("facebook", facebook);
//    	sub.put("twitter", twitter);
//    	return sub;
//    }

    public static String getStrDate() 
    {
    	GregorianCalendar c = new GregorianCalendar();
    	int m = c.get(GregorianCalendar.MONTH) + 1;
    	int d = c.get(GregorianCalendar.DATE);
    	String mm = Integer.toString(m);
    	String dd = Integer.toString(d);
    	return "" + c.get(GregorianCalendar.YEAR) + (m < 10 ? "0" + mm : mm) +
    			(d < 10 ? "0" + dd : dd);
    }
    
}
