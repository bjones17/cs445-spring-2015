package user;

import java.io.Serializable;
import java.util.GregorianCalendar;

public class Admin implements Serializable
{
	private int ID;
	private String name;
	private String date;
	
	public Admin()
	{
		this.name = "John Doe";
		this.ID = IdGenerator.newID();
		this.date = getStrDate();
	}
	
	public Admin(String name)
	{
		this.name = name;
		this.ID = IdGenerator.newID();
		this.date = getStrDate();
	}
	
	public int getID()
	{
		return this.ID;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String getDateCreated()
	{
		return this.date;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public static String getStrDate() 
    {
    	GregorianCalendar c = new GregorianCalendar();
    	int m = c.get(GregorianCalendar.MONTH) + 1;
    	int d = c.get(GregorianCalendar.DATE);
    	String mm = Integer.toString(m);
    	String dd = Integer.toString(d);
    	return "" + c.get(GregorianCalendar.YEAR) + (m < 10 ? "0" + mm : mm) +
    			(d < 10 ? "0" + dd : dd);
    }
}
