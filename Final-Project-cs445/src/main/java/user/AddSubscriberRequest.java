package user;


import java.util.Collection;

// Boundary class for a request to add a subscriber
public abstract class AddSubscriberRequest {
	
	String name, email, phone, address, twitter, facebook;
	
	public AddSubscriberRequest(String address, String name, String email, String phone, String tw, String fb) {
		this.address = address;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.twitter = tw;
		this.facebook = fb;
	}
	
	public abstract CustomResponse addAccount(Collection<Subscriber> subs);
}
