package user;

public class Receipt {

	private String recipient;
	private int ID;
	
	public Receipt(){
		recipient = "Virgil Bistriceanu";
		ID = IdGenerator.newID();
	}
	
	public Receipt(String name){
		recipient = name;
		ID = IdGenerator.newID();
	}
	
	public String getRecipient(){
		return this.recipient;
	}
	
	public int getID(){
		return this.ID;
	}
}