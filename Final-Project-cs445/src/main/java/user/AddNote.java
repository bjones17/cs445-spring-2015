package user;


public class AddNote extends AddNoteRequest{
	
	public AddNote(String m){
		super(m);
	}
	
	public CustomResponse addNote(Shipment shipment){
		
		if(message.length()<128){
			return new AddNoteResponse(false, "Message must be more than 128 characters");
		} else if(message.length()>1024){
			return new AddNoteResponse(false, "Message must be less than 1024 characters");
		}
		shipment.addNote(message);
		return new AddNoteResponse(true, "Note successfully added");
	}
}