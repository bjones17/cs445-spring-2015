package user;

import java.util.Collection;

public class AddSubscriber extends AddSubscriberRequest {
	private Subscriber sub;
	private String[] bannedStates= {"Alabama",
						"Arkansas",
	                    "Delaware",
	                    "Kentucky",
	                    "Massachusetts",
	                    "Mississippi",
	                    "Oklahoma",
	                    "Pennsylvania",
	                    "South Dakota",
	                    "Utah"};

	public AddSubscriber(String address, String name, String email, String phone, String tw, String fb) {
		super(address, name, email, phone, tw, fb);
	}
	
	@Override
	public CustomResponse addAccount(Collection<Subscriber> subs) {
		sub = new Subscriber (this.name, this.email, this.phone, this.address, this.facebook, this.twitter);
		
		if (addressInBannedState()) {
			return new AddSubscriberResponse(0, false, "Cannot ship to this state");
		}
		
		if (userHasAccount(subs)) {
			return new AddSubscriberResponse(0, false, "User already has an account");
		} else {
			subs.add(sub);
			return new AddSubscriberResponse(sub.getID(), true, "Congratulations");
		}
	}
	
	private boolean userHasAccount(Collection<Subscriber> subs) {
		for(Subscriber e : subs){
			if(e.isMatch(this.name) ||
				e.isMatch(this.email) ||
				e.isMatch(this.phone))
				return true;
		}
		return false;
	}
	
	private boolean addressInBannedState() {
		for(String s : bannedStates){
			if(this.address.toLowerCase().contains(s.toLowerCase()))
				return true;
		}
		return false;
	}
	
}
