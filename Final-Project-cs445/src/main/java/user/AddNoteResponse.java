package user;


public class AddNoteResponse extends CustomResponse{

	public AddNoteResponse(boolean status, String feedback){
		super(status, feedback);
	}
	
	public void printResponse(){
		System.out.println("Status: " + status + "\tDescription: " + feedback);
	}
}
