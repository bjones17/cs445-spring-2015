package wine;

import enums.MonthlySelectionType;

public class AR extends MonthlySelection {

	public AR(){
		super.mst = MonthlySelectionType.AR;
	}
	
	public void addWine(Wine w){
		//Make sure only Red wines are added
		if((w.getVariety().equals(WineVariety.RED) 
				|| w.getVariety().equals(WineVariety.ROSE))
				&& ms.size()<6){
			ms.add(w);
		}
	}
}
