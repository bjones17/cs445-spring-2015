package wine;

import enums.Day;
import enums.AMPM;

public class Delivery {
    private Day day;
    private AMPM time;
    
    public Delivery(Day day, AMPM time){
    	this.day = day;
    	this.time = time;
    }

    public Day getDow(){
        return this.day;
    }

    public AMPM getTime(){
    	return this.time;
    }
}
