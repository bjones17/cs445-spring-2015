package wine;

import enums.MonthlySelectionType;

public class AW extends MonthlySelection {

	public AW() {
		super.mst = MonthlySelectionType.AW;
	}
	
	@Override
	public void addWine(Wine w) {
		// Make sure only white wines are added
		if(w.getVariety().equals(WineVariety.WHITE) && ms.size()<6){
			ms.add(w);
		}
	}

}
