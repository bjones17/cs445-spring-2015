package wine;

import enums.MonthlySelectionType;

public class RW extends MonthlySelection {

	public RW(){
		super.mst = MonthlySelectionType.RW;
	}
	
	public void addWine(Wine w){
			//Make sure 3 Reds, 3 Whites
		if(ms.size()<6){
			if((w.getVariety().equals(WineVariety.RED) || w.getVariety().equals(WineVariety.ROSE))
					&& countReds()<3)
				ms.add(w);
			if(w.getVariety().equals(WineVariety.WHITE) && countWhites()<3)
				ms.add(w);
		}
	}
	
	int countReds(){
		int count=0;
		for(Wine w : ms){
			if(w.getVariety().equals(WineVariety.RED)
					|| w.getVariety().equals(WineVariety.ROSE))
				count++;
		}
		return count;
	}
	
	int countWhites(){
		int count=0;
		for(Wine w : ms){
			if(w.getVariety().equals(WineVariety.WHITE))
				count++;
		}
		return count;
	}
}
