package wine;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.time.YearMonth;

import user.IdGenerator;
import enums.MonthlySelectionType;


public abstract class MonthlySelection {
	protected MonthlySelectionType mst;
	private YearMonth ym;
	protected List<Wine> ms = new ArrayList<Wine>();
	private int ID;
	
	public abstract void addWine(Wine w);
	
	public boolean isMatch(String kw) {
		Iterator<Wine> it = this.ms.iterator();
		while (it.hasNext()) {
			Wine w = it.next();
			if (w.isMatch(kw)) return true;
		}
		return false;
	}
	
	public MonthlySelection() {
		this.ym = YearMonth.now().plusMonths(1);	// next month's selection
		ID = IdGenerator.newID();
	}
	
	public int getID(){
		return this.ID;
	}
	
	public YearMonth getYearMonth(){
		return this.ym;
	}
	
	public MonthlySelectionType getType(){
		return this.mst;
	}
	
	public List<Wine> getWineList(){
		return this.ms;
	}
}
