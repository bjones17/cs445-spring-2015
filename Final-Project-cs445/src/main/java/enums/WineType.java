package enums;


public enum WineType {
	TABLE, 
	SWEET, 
	SPARKLING;
}
