package enums;


public enum DeliveryStatus {
	DELIVERED, SHIPPED, RETURNED, CANCELLED, PENDING;
}
