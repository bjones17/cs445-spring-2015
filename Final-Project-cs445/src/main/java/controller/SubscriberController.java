package controller;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import enums.Day;
import enums.AMPM;
import enums.MonthlySelectionType;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import user.AddSubscriber;
import user.AddSubscriberResponse;
import user.Admin;
import user.CustomResponse;
import user.Note;
import user.Shipment;
import user.Subscriber;
import wine.AR;
import wine.Delivery;
import wine.Wine;

@RestController
@RequestMapping("/vin/sub")
public class SubscriberController {
	
	private static Collection<Subscriber> subsCollection = new ArrayList<Subscriber>();
	private static Collection<Note> noteCollection = new ArrayList<Note>();
	
	public static Collection<Subscriber> getSubscriberCollection(){
		return subsCollection;
	}
	
	public static Collection<Note> getNotesCollection(){
		return noteCollection;
	}
	
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<String> addSubscriber(@RequestParam(value = "email") String email,
                                   							  @RequestParam(value = "name") String name,
                                   							  @RequestParam(value = "phone") String phone,
                                   							  @RequestParam(value = "address") String address,
                                   							  @RequestParam(value = "facebook", required = false, defaultValue = "N/A") String fb,
                                   							  @RequestParam(value = "twitter", required = false, defaultValue = "N/A") String tw){
    	
    	JSONObject JSON_Response = new JSONObject();
    	
    	AddSubscriber addSub = new AddSubscriber(address, name, email, phone, fb, tw);
    	
    	AddSubscriberResponse result = (AddSubscriberResponse) addSub.addAccount(subsCollection);
    	
    	JSON_Response.put("id", result.getID());
    	JSON_Response.put("errors", result.getFeedback());
    	
        return new ResponseEntity<String>(JSON_Response.toString(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{uid}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Subscriber> getSubscriber(@PathVariable(value = "uid") int uid){
    	Iterator<Subscriber> subIterator = subsCollection.iterator();
        Subscriber tempSub = null;
        
        while(subIterator.hasNext()){
        	tempSub = subIterator.next();
        	
        	if(tempSub.getID() == uid){
        		return new ResponseEntity<Subscriber>(tempSub, HttpStatus.OK);
        	}
        }
        return new ResponseEntity<Subscriber>(HttpStatus.NOT_FOUND);
    }

//    @RequestMapping(value = "/{uid}/search", method = RequestMethod.GET)
//    public @ResponseBody ResponseEntity<Object> query(@PathVariable(value = "uid") int uid,
//                          				@RequestParam(value = "q", required = false) String query){
//        return new ResponseEntity<Object>(obj, HttpStatus.OK);
//    }  ***Very high complexity

    @RequestMapping(value = "/{uid}/shipments", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<ArrayList<Shipment>> getShipments(@PathVariable(value = "uid") int uid){
    	Iterator<Subscriber> subIterator = subsCollection.iterator();
        Subscriber tempSub = null;
        
        while(subIterator.hasNext()){
        	tempSub = subIterator.next();
        	
        	if(tempSub.getID() == uid){
        		return new ResponseEntity<ArrayList<Shipment>>((ArrayList<Shipment>)tempSub.getShipments(), HttpStatus.OK);
        	}
        }
        return new ResponseEntity<ArrayList<Shipment>>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{uid}/shipments/{sid}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Shipment> getSpecificShipment(@PathVariable(value = "uid") int uid,
                                        							  @PathVariable(value = "sid") int sid){
    	Iterator<Subscriber> subIterator = subsCollection.iterator();
        Subscriber tempSub = null;
        
        while(subIterator.hasNext()){
        	tempSub = subIterator.next();
        	
        	if(tempSub.getID() == uid){
        		Iterator<Shipment> shipmentIterator = tempSub.getShipments().iterator();
        		Shipment tempShipment = null;
        		
        		while(shipmentIterator.hasNext()){
        			tempShipment = shipmentIterator.next();
                	
                	if(tempShipment.getID() == sid){
                		return new ResponseEntity<Shipment>(tempShipment, HttpStatus.OK);
                	}
                }
        		return new ResponseEntity<Shipment>(HttpStatus.NOT_FOUND);
        	}
        }
        return new ResponseEntity<Shipment>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{uid}/shipments/{sid}/notes", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<ArrayList<Note>> getNotes(@PathVariable(value = "uid") int uid,
    															  @PathVariable(value = "sid") int sid){
    	Iterator<Subscriber> subIterator = subsCollection.iterator();
        Subscriber tempSub = null;
        
        while(subIterator.hasNext()){
        	tempSub = subIterator.next();
        	
        	if(tempSub.getID() == uid){
        		Iterator<Shipment> shipmentIterator = tempSub.getShipments().iterator();
        		Shipment tempShipment = null;
        		
        		while(shipmentIterator.hasNext()){
        			tempShipment = shipmentIterator.next();
                	
                	if(tempShipment.getID() == sid){
                		return new ResponseEntity<ArrayList<Note>>((ArrayList<Note>) tempShipment.getNotes(), HttpStatus.OK);
                	}
                }
        		return new ResponseEntity<ArrayList<Note>>(HttpStatus.NOT_FOUND);
        	}
        }
        return new ResponseEntity<ArrayList<Note>>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{uid}/shipments/{sid}/notes", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Integer> addNote(@PathVariable(value = "uid") int uid,
    													 @PathVariable(value = "sid") int sid,
    													 @RequestParam(value = "content") String content){
    	Iterator<Subscriber> subIterator = subsCollection.iterator();
        Subscriber tempSub = null;
        
        while(subIterator.hasNext()){
        	tempSub = subIterator.next();
        	
        	if(tempSub.getID() == uid){
        		Iterator<Shipment> shipmentIterator = tempSub.getShipments().iterator();
        		Shipment tempShipment = null;
        		
        		while(shipmentIterator.hasNext()){
        			tempShipment = shipmentIterator.next();
                	
                	if(tempShipment.getID() == sid){
                		int id = tempShipment.addNote(content);
                		return new ResponseEntity<Integer>(new Integer(id), HttpStatus.OK);
                	}
                }
        		return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);
        	}
        }
        return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{uid}/shipments/{sid}/notes/{nid}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Note> getShipmentNote(@PathVariable(value = "uid") int uid,
    														  @PathVariable(value = "sid") int sid,
    														  @PathVariable(value = "nid") int nid){
    	Iterator<Subscriber> subIterator = subsCollection.iterator();
        Subscriber tempSub = null;
        
        while(subIterator.hasNext()){
        	tempSub = subIterator.next();
        	
        	if(tempSub.getID() == uid){
        		Iterator<Shipment> shipmentIterator = tempSub.getShipments().iterator();
        		Shipment tempShipment = null;
        		
        		while(shipmentIterator.hasNext()){
        			tempShipment = shipmentIterator.next();
                	
                	if(tempShipment.getID() == sid){
                		Iterator<Note> noteIterator = tempShipment.getNotes().iterator();
                		Note tempNote = null;
                		
                		while(noteIterator.hasNext()){
                			tempNote = noteIterator.next();
                        	
                        	if(tempNote.getID() == nid){
                        		return new ResponseEntity<Note>(tempNote, HttpStatus.OK);
                        	}
                        }
                		return new ResponseEntity<Note>(HttpStatus.NOT_FOUND);
                	}
                }
        		return new ResponseEntity<Note>(HttpStatus.NOT_FOUND);
        	}
        }
        return new ResponseEntity<Note>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{uid}/shipments/{sid}/notes/{nid}", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Object> deleteNote(@PathVariable(value = "uid") int uid,
                           @PathVariable(value = "sid") int sid,
                           @PathVariable(value = "nid") int nid){
    	Iterator<Subscriber> subIterator = subsCollection.iterator();
        Subscriber tempSub = null;
        
        while(subIterator.hasNext()){
        	tempSub = subIterator.next();
        	
        	if(tempSub.getID() == uid){
        		Iterator<Shipment> shipmentIterator = tempSub.getShipments().iterator();
        		Shipment tempShipment = null;
        		
        		while(shipmentIterator.hasNext()){
        			tempShipment = shipmentIterator.next();
                	
                	if(tempShipment.getID() == sid){
                		Iterator<Note> noteIterator = tempShipment.getNotes().iterator();
                		Note tempNote = null;
                		
                		while(noteIterator.hasNext()){
                			tempNote = noteIterator.next();
                        	
                        	if(tempNote.getID() == nid){
                        		tempShipment.getNotes().remove(tempNote);
                        		return new ResponseEntity<Object>(HttpStatus.OK);
                        	}
                        }
                    	return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
                	}
                }
            	return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        	}
        }
        return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{uid}/wines", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<String> getWinesBySubscriber(@PathVariable(value = "uid") int uid){
    	Iterator<Subscriber> subIterator = subsCollection.iterator();
        Subscriber tempSub = null;
        JSONArray wines = new JSONArray();
        
        while(subIterator.hasNext()){
        	tempSub = subIterator.next();
        	
        	if(tempSub.getID() == uid){
        		for(Shipment s : tempSub.getShipments()){
        			wines.put(s.getMs().getWineList());
        		}
        		return new ResponseEntity<String>(wines.toString(), HttpStatus.OK);
        	}
        }
        return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{uid}/wines/{wid}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Wine> getWineBySubscriber(@PathVariable(value = "uid") int uid,
    												  @PathVariable(value = "wid") int wid){
    	Iterator<Subscriber> subIterator = subsCollection.iterator();
        Subscriber tempSub = null;
        
        while(subIterator.hasNext()){
        	tempSub = subIterator.next();
        	
        	if(tempSub.getID() == uid){
        		for(Shipment s : tempSub.getShipments()){
        			for(Wine w : s.getMs().getWineList()){
        				if(w.getID() == wid){
        					return new ResponseEntity<Wine>(w, HttpStatus.OK);
        				}
        			}
        		}
        		return new ResponseEntity<Wine>(HttpStatus.NOT_FOUND);
        	}
        }
        return new ResponseEntity<Wine>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{uid}/wines/{wid}/notes", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<String> getNotesBySubscriber(@PathVariable(value = "uid") int uid,
                                 									 @PathVariable(value = "wid") int wid){
    	Iterator<Subscriber> subIterator = subsCollection.iterator();
        Subscriber tempSub = null;
        JSONArray notes = new JSONArray();
        
        while(subIterator.hasNext()){
        	tempSub = subIterator.next();
        	
        	if(tempSub.getID() == uid){
        		for(Shipment s : tempSub.getShipments()){
        			for(Wine w : s.getMs().getWineList()){
        				if(w.getID() == wid){
        					notes.put(w.getNotes());
        				}
        			}
        		}
        		return new ResponseEntity<String>(notes.toString(), HttpStatus.OK);
        	}
        }
        return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{uid}/wines/{wid}/notes", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Integer> addWineNote(@PathVariable(value = "uid") int uid,
    														 @PathVariable(value = "wid") int wid,
    														 @RequestParam(value = "content") String content){
    	Iterator<Subscriber> subIterator = subsCollection.iterator();
        Subscriber tempSub = null;
        
        while(subIterator.hasNext()){
        	tempSub = subIterator.next();
        	
        	if(tempSub.getID() == uid){
        		for(Shipment s : tempSub.getShipments()){
        			for(Wine w : s.getMs().getWineList()){
        				if(w.getID() == wid){
        					int id = w.addNote(content);
        					return new ResponseEntity<Integer>(new Integer(id), HttpStatus.CREATED);
        				}
        			}
        		}
        		return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);
        	}
        }
        return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{uid}/wines/{wid}/notes/{nid}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Note> getNoteByUid(@PathVariable(value = "uid") int uid,
    													   @PathVariable(value = "wid") int wid,
    													   @PathVariable(value = "nid") int nid){
    	Iterator<Subscriber> subIterator = subsCollection.iterator();
        Subscriber tempSub = null;
        
        while(subIterator.hasNext()){
        	tempSub = subIterator.next();
        	
        	if(tempSub.getID() == uid){
        		for(Shipment s : tempSub.getShipments()){
        			for(Wine w : s.getMs().getWineList()){
        				for(Note n : w.getNotes()){
        					if(n.getID() == nid){
        						return new ResponseEntity<Note>(n, HttpStatus.OK);
        					}
        				}
        			}
        		}
        		return new ResponseEntity<Note>(HttpStatus.NOT_FOUND);
        	}
        }
        return new ResponseEntity<Note>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{uid}/wines/{wid}/notes/{nid}", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Object> removeNoteByUid(@PathVariable(value = "uid") int uid,
                                @PathVariable(value = "wid") int wid,
                                @PathVariable(value = "nid") int nid){
    	Iterator<Subscriber> subIterator = subsCollection.iterator();
        Subscriber tempSub = null;
        
        while(subIterator.hasNext()){
        	tempSub = subIterator.next();
        	
        	if(tempSub.getID() == uid){
        		for(Shipment s : tempSub.getShipments()){
        			for(Wine w : s.getMs().getWineList()){
        				for(Note n : w.getNotes()){
        					if(n.getID() == nid){
        						w.getNotes().remove(n);
        						return new ResponseEntity<Object>(HttpStatus.OK);
        					}
        				}
        			}
        		}
        		return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        	}
        }
        return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{uid}/wines/{wid}/rating", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Float> getRating(@PathVariable(value = "uid") int uid,
    													 @PathVariable(value = "wid") int wid){
        for(Subscriber sub : subsCollection){
        	if(sub.getID() == uid){
        		for(Shipment shipment : sub.getShipments()){
        			for(Wine w : shipment.getMs().getWineList()){
        				if(w.getID() == wid){
        					return new ResponseEntity<Float>(new Float(w.getRating()), HttpStatus.OK);
        				}
        			}
        		}
        		return new ResponseEntity<Float>(HttpStatus.NOT_FOUND);
        	}
        }
        return new ResponseEntity<Float>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{uid}/wines/{wid}/rating", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Object> submitRating(@PathVariable(value = "uid") int uid,
                             @PathVariable(value = "wid") int wid,
                             @RequestParam(value = "rating") int rating){
    	for(Subscriber sub : subsCollection){
        	if(sub.getID() == uid){
        		for(Shipment shipment : sub.getShipments()){
        			for(Wine w : shipment.getMs().getWineList()){
        				if(w.getID() == wid){
        					w.addRating(rating);
        					return new ResponseEntity<Object>(HttpStatus.OK);
        				}
        			}
        		}
        		return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        	}
        }
        return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/{uid}/delivery", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Delivery> getDelivery(@PathVariable(value = "uid") int uid){
        for(Subscriber sub : subsCollection){
        	if(sub.getID() == uid){
        		return new ResponseEntity<Delivery>(sub.getDelivery(), HttpStatus.OK);
        	}
        }
        return new ResponseEntity<Delivery>(HttpStatus.NOT_FOUND);
    }
}
