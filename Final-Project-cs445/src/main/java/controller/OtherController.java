package controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import user.Admin;
import wine.Wine;

@RestController
@RequestMapping("/wines")
public class OtherController {

	private static Collection<Wine> wineCollection = new ArrayList<Wine>();
	
    @RequestMapping(value = "/{wid}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Wine> getWine(@PathVariable(value = "wid") int wid){
    	Iterator<Wine> wineIterator = wineCollection.iterator();
        Wine tempWine = null;
        
        while(wineIterator.hasNext()){
        	tempWine = wineIterator.next();
        	
        	if(tempWine.getID() == wid){
        		return new ResponseEntity<Wine>(tempWine, HttpStatus.OK);
        	}
        }
        return new ResponseEntity<Wine>(HttpStatus.NOT_FOUND);
    }
}
