package controller;


import enums.WineType;
import enums.MonthlySelectionType;

import org.springframework.web.bind.annotation.*;

import user.Admin;
import wine.AR;
import wine.AW;
import wine.MonthlySelection;
import wine.RW;
import wine.Wine;

import java.io.Serializable;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.json.*;

@RestController
@RequestMapping("/vin/admin")
public class AdminController {

	private static Collection<Admin> adminCollection = new ArrayList<Admin>();
	private static Collection<MonthlySelection> msCollection = new ArrayList<MonthlySelection>();
	
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Admin> createAdmin(@RequestParam(value = "name") String name){
    	Admin adminObj = new Admin(name);
        adminCollection.add(adminObj);
        
        return new ResponseEntity<Admin>(adminObj, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<String> getAdmins(){
    	JSONArray adminArray = new JSONArray();
		Iterator<Admin> adminIterator = adminCollection.iterator();
		Admin tempAdmin = null;

		while(adminIterator.hasNext()){
			tempAdmin = adminIterator.next();

			try{
				adminArray.put(new JSONObject().put("id", tempAdmin.getID()).put("name", tempAdmin.getName()));
			}
			catch(Exception e){
				return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
			}
		}

		return new ResponseEntity<String>(adminArray.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{aid}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Admin> getAdmin(@PathVariable(value = "aid") int aid){
        Iterator<Admin> adminIterator = adminCollection.iterator();
        Admin tempAdmin = null;
        
        while(adminIterator.hasNext()){
        	tempAdmin = adminIterator.next();
        	
        	if(tempAdmin.getID() == aid){
        		return new ResponseEntity<Admin>(tempAdmin, HttpStatus.OK);
        	}
        }
        return new ResponseEntity<Admin>(HttpStatus.NOT_FOUND);
    }

//    @RequestMapping(value = "/revenue", method = RequestMethod.GET)
//    public Revenue getRevenue(@RequestParam(value = "start", required = false) String start,
//                              @RequestParam(value = "end", required = false) String end){
//        return new Revenue();
//    }  Not sure how to do this

    @RequestMapping(value = "/monthly_selection", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<String> getMonthlySelectionIds(){
    	JSONArray msArray = new JSONArray();
		Iterator<MonthlySelection> msIterator = msCollection.iterator();
		MonthlySelection tempSelection = null;

		while(msIterator.hasNext()){
			tempSelection = msIterator.next();

			try{
				msArray.put(new JSONObject().put("id", tempSelection.getID()).put("selection_month", tempSelection.getYearMonth().toString()).put("type", tempSelection.getType().toString()));
			}
			catch(Exception e){
				return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
			}
		}
		
		return new ResponseEntity<String>(msArray.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/monthly_selection", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Integer> createMonthlySelection(@RequestParam(value = "type") MonthlySelectionType type,
                                      @RequestParam(value = "selection_month") String selection_month,
                                      @RequestParam(value = "wines") Wine[] wines){
        MonthlySelection ms = null;
    	
    	switch(type){
        case AR:
        	ms = new AR();
        	break;
        	
        case RW:
        	ms = new RW();
        	break;
        	
        case AW:
        	ms = new AW();
        	break;
        	
        default:
        	ms = new RW();
        	break;
        }
    	
    	for(int i=0; i>wines.length; i++){
    		ms.addWine(wines[i]);
    	}
    	
    	msCollection.add(ms);
    	
    	return new ResponseEntity<Integer>(new Integer(ms.getID()), HttpStatus.OK);
    }

    @RequestMapping(value = "/monthly_selection/{mid}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<MonthlySelection> getMonthlySelection(@PathVariable(value = "mid") int mid){
    	Iterator<MonthlySelection> msIterator = msCollection.iterator();
        MonthlySelection tempSelection = null;
        
        while(msIterator.hasNext()){
        	tempSelection = msIterator.next();
        	
        	if(tempSelection.getID() == mid){
        		return new ResponseEntity<MonthlySelection>(tempSelection, HttpStatus.OK);
        	}
        }
        return new ResponseEntity<MonthlySelection>(HttpStatus.NOT_FOUND);
    }
}
