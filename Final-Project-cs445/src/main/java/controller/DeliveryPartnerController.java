package controller;

import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import enums.DeliveryStatus;
import user.Receipt;
import user.Shipment;
import user.Subscriber;

@RestController
@RequestMapping("/vin")
public class DeliveryPartnerController {
	
	private static Collection<Receipt> receiptCollection = new ArrayList<Receipt>();

    @RequestMapping(value = "/partner", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<String> getPendingDeliveries(){
        JSONArray JSON_Subs = new JSONArray();
        
        for(Subscriber s : SubscriberController.getSubscriberCollection()){
        	for(Shipment shipment : s.getShipments()){
        		if(shipment.getStatus() == DeliveryStatus.PENDING){
        			JSON_Subs.put(s);
        			break;
        		}
        	}
        }
        
        return new ResponseEntity<String>(JSON_Subs.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/receipt", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Receipt> createReceipt(@RequestParam(value = "name") String name){
        Receipt r = new Receipt(name);
        return new ResponseEntity<Receipt>(r, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/receipt", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<String> getReceipts(){
        JSONArray receipts = new JSONArray();
        
        receipts.put(receiptCollection);
        
        return new ResponseEntity<String>(receipts.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/receipt/{rid}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Receipt> getReceipt(@PathVariable(value = "rid") int rid){
        for(Receipt r : receiptCollection){
        	if(r.getID() == rid){
        		return new ResponseEntity<Receipt>(r, HttpStatus.OK);
        	}
        }
        return new ResponseEntity<Receipt>(HttpStatus.NOT_FOUND);
    }

}
