package core;

import static org.junit.Assert.*;

import org.junit.Test;

import user.Admin;

public class AdminTest {

	@Test
	public void test_constructor(){
		Admin a = new Admin();
		Admin b = new Admin();
		
		assertEquals(a.getName(), "John Doe");
		assertFalse(a.getID() == b.getID());
	}

	@Test
	public void test_overloaded_constructor(){
		Admin a = new Admin("Alice Admin");
		
		assertEquals(a.getName(), "Alice Admin");
	}
}
