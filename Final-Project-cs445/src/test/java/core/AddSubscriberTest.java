package core;


import static org.junit.Assert.*;

import org.junit.Test; 

import user.AddSubscriber;
import user.Subscriber;

import java.util.Collection;
import java.util.ArrayList;

public class AddSubscriberTest {
	
	private final String STATE_ERR = "Cannot ship to this state";
	private final String EXISTS_ERR = "User already has an account";
	private final String SUCCESS = "Congratulations";

	Collection<Subscriber> subs = new ArrayList<Subscriber>();
	
	@Test
	public void test_add_account(){ //Tests userHasAccount and addressInBannedState as well
		AddSubscriber add = new AddSubscriber("123 Easy St Easydale Illinois 12345", "John Doe", "jdoe@email.com", "1234567890",
				"jdoe", "johndoe");
		//AddSubscriberResponse asr = new AddSubscriberResponse(1, true, "Congratulations");
		assertEquals(add.addAccount(subs).getFeedback(), SUCCESS); 	//Test a success
		assertEquals(add.addAccount(subs).getFeedback(), EXISTS_ERR);//Account exists
		subs.clear();
		assertTrue(add.addAccount(subs).getStatus());
		subs.clear();
		add = new AddSubscriber("123 Easy St Easydale Alabama 12345", "John Doe", "jdoe@email.com", "1234567890",
				"jdoe", "johndoe");
		assertFalse(add.addAccount(subs).getStatus()); 				//Tests Alabama, banned state
		assertEquals(add.addAccount(subs).getFeedback(), STATE_ERR);
	}

}
