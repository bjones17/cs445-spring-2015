package core;


import static org.junit.Assert.*;

import java.time.DayOfWeek;

import org.junit.Test;

import enums.DeliveryStatus;
import user.AddNote;
import user.CustomResponse;
import user.Shipment;
import user.Subscriber;
import wine.AR;

public class AddNoteTest {
	
	private final String SHORT_ERR = "Message must be more than 128 characters";
	private final String LONG_ERR = "Message must be less than 1024 characters";
	private final String SUCCESS = "Note successfully added";

	String longString = "";{
		for(int i = 0; i<1230; i++)
			longString += "A";
	}

	String goodString = "";{
		for(int i = 0; i<130; i++)
			goodString += "A";
	}
	
	@Test
	public void test_add_note() {
		Shipment shipment = new Shipment(new Subscriber(), DayOfWeek.MONDAY, new AR(), DeliveryStatus.PENDING);
		AddNote shortNote = new AddNote("Short note");
		AddNote longNote = new AddNote(longString);
		AddNote goodNote = new AddNote(goodString);
		CustomResponse response = shortNote.addNote(shipment);
		assertFalse(response.getStatus());				//Tests a note < 128 char
		assertEquals(response.getFeedback(), SHORT_ERR);
		response = longNote.addNote(shipment);
		assertFalse(response.getStatus());				//Tests a note > 1024 char
		assertEquals(response.getFeedback(), LONG_ERR);
		response = goodNote.addNote(shipment);
		assertTrue(response.getStatus());				//Tests a successful add
		assertEquals(response.getFeedback(), SUCCESS);
	}

}
