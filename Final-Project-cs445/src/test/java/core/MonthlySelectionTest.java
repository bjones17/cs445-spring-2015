package core;


import static org.junit.Assert.*;

import org.junit.Test;

import enums.WineType;
import wine.AR;
import wine.AW;
import wine.MonthlySelection;
import wine.RW;
import wine.Wine;
import wine.WineVariety;

import java.time.Year;

public class MonthlySelectionTest {

	MonthlySelection ar = new AR(), aw = new AW(), rw = new RW(),
			fullRed = new AR(), fullWhite = new AW(), fullMix = new RW();
	Wine white = new Wine(WineVariety.WHITE, WineType.TABLE, "ln", "g", "r", "c", "m", Year.parse("2011")),
		 rose = new Wine(WineVariety.ROSE, WineType.TABLE, "ln", "g", "r", "c", "m", Year.parse("2011")),
		 red = new Wine(),
		 white2 = new Wine(WineVariety.WHITE, WineType.SPARKLING, "ln", "g", "r", "c", "m", Year.parse("2011"));
	
	@Test
	public void test_add_wine() {
		ar.addWine(red);
		assertTrue(ar.getWineList().contains(red));
		ar.addWine(rose);
		assertTrue(ar.getWineList().contains(rose));
		ar.addWine(white);
		assertFalse(ar.getWineList().contains(white));
		
		aw.addWine(red);
		assertFalse(aw.getWineList().contains(red));
		aw.addWine(rose);
		assertFalse(aw.getWineList().contains(rose));
		aw.addWine(white);
		assertTrue(aw.getWineList().contains(white));
		
		rw.addWine(red);
		assertTrue(rw.getWineList().contains(red));
		rw.addWine(rose);
		assertTrue(rw.getWineList().contains(rose));
		rw.addWine(white);
		assertTrue(rw.getWineList().contains(white));
		
		for(int i = 0; i<6; i++)
			fullRed.addWine(red);
		fullRed.addWine(rose);
		assertFalse(fullRed.getWineList().contains(rose));
		
		for(int i = 0; i<6; i++)
			fullWhite.addWine(white);
		fullWhite.addWine(white2);
		assertFalse(fullWhite.getWineList().contains(white2));
		
		for(int i = 0; i<6; i++)
			fullMix.addWine(red);
		fullMix.addWine(rose);
		assertFalse(fullMix.getWineList().contains(rose));
	}
	
	@Test
	public void test_is_match(){
		MonthlySelection sel = new AR();
		for(int i=0; i<6; i++)
			sel.addWine(new Wine());
		assertTrue(sel.isMatch("The Mission"));
		assertTrue(sel.isMatch("Cabernet Sauvignon"));
		assertTrue(sel.isMatch("Napa"));
		assertTrue(sel.isMatch("USA"));
		assertTrue(sel.isMatch("Sterling"));
		assertTrue(sel.isMatch("2011"));
		assertFalse(sel.isMatch("Italy"));
	}
	
	@Test
	public void test_ID_functionality(){
		MonthlySelection sel1 = new RW();
		MonthlySelection sel2 = new AR();
		
		assertFalse(sel1.getID() == sel2.getID());
	}
	
	@Test
	public void test_type_instantiation(){
		MonthlySelection sel1 = new RW();
		MonthlySelection sel2 = new AR();
		
		assertFalse(sel1.getType().equals(sel2.getType()));
	}
}
