package core;


import static org.junit.Assert.*;

import org.junit.Test;
import org.json.*;

import enums.MonthlySelectionType;
import user.Subscriber;

import java.util.GregorianCalendar;

public class SubscriberTest {

    @Test
    public void test_search_match_name() {
    	Subscriber s = new Subscriber();
    	assertTrue(s.isMatch("jane"));
    }
    
    @Test
    public void test_search_fails() {
    	Subscriber s = new Subscriber();
    	assertFalse(s.isMatch("smith"));
    }
    
    @Test
    public void test_search_match_email() {
    	Subscriber s = new Subscriber();
    	assertTrue(s.isMatch("example.com"));
    }       

    @Test
    public void test_search_match_phone_partial() {
    	Subscriber s = new Subscriber();
    	assertTrue(s.isMatch("7890"));
    }
    
    @Test
    public void test_formatted_phone_number_matches() {
    	Subscriber s = new Subscriber();
    	assertTrue(s.isMatch("456-7890"));
    }
    
    @Test
    public void test_search_match_phone_full() {
    	Subscriber s = new Subscriber("John Doe", "johndoe@example.com", "(123) 456-7890", "123 AnyStreet Anytown AnyState 56789");
    	assertTrue(s.isMatch("1234567890"));
    }
    
    @Test
    public void test_default_monthly_preference() {
    	Subscriber s = new Subscriber();
    	assertEquals(MonthlySelectionType.RW, s.getPreference());
    }
    
    @Test
    public void test_changing_the_monthly_preference() {
    	Subscriber s = new Subscriber();
    	s.setPreference(MonthlySelectionType.AR);
    	assertEquals(MonthlySelectionType.AR, s.getPreference());
    }
    
//    @Test
//    public void test_toJSON(){
//    	Subscriber s = new Subscriber();
//    	JSONObject json = new JSONObject();
//    	json.put("uid", s.getID());
//    	GregorianCalendar c = new GregorianCalendar();
//    	int m = c.get(GregorianCalendar.MONTH) + 1;
//    	int d = c.get(GregorianCalendar.DATE);
//    	String mm = Integer.toString(m);
//    	String dd = Integer.toString(d);
//    	String date = "" + c.get(GregorianCalendar.YEAR) + (m < 10 ? "0" + mm : mm) +
//    			(d < 10 ? "0" + dd : dd);
//    	json.put("date_created", s.getDateCreated());
//    	json.put("type", s.getPreference());
//    	json.put("name", "Jane Doe");
//    	json.put("email", "jane.doe@example.com");
//    	json.put("phone", "1234567890");
//    	JSONObject addr = new JSONObject();
//    	addr.put("street", "123 Main ST, Apt 2F");
//    	addr.put("city", "Anytown");
//    	addr.put("state", "Anystate");
//    	addr.put("zip", "12345");
//    	json.put("address", addr);
//    	json.put("facebook", "");
//    	json.put("twitter", "");
//    	assertTrue(json.equals(s.toJSON()));
//    }
}
