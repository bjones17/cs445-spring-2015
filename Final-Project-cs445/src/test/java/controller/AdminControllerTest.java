package controller;
import application.Application;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
@WebAppConfiguration
public class AdminControllerTest {

    @Autowired
    private WebApplicationContext webAppConfiguration;

    private MockMvc mockMvc;

    @Autowired
    SubscriberController subscriberController;

    @Before
    public void setUp(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webAppConfiguration).build();
    }

    @Test
    public void test_admin_post() throws Exception{

        MvcResult result = mockMvc.perform(post("/vin/admin")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("name", "Alice Admin")
        ).andExpect(status().isCreated()).andReturn();
    }
    
    @Test
    public void test_admin_get() throws Exception{

        MvcResult result = mockMvc.perform(post("/vin/admin")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("name", "Alice Admin")
        ).andExpect(status().isCreated()).andReturn();
        
        String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(get("/vin/admin")
                .contentType(MediaType.APPLICATION_JSON)
    	).andExpect(status().isOk()).andReturn();
    }
    
    @Test
    public void test_admin_getByID() throws Exception{

        MvcResult result = mockMvc.perform(post("/vin/admin")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("name", "Alice Admin")
        ).andExpect(status().isCreated()).andReturn();
        
        String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(get("/vin/admin/" + idNum)
                .contentType(MediaType.APPLICATION_JSON)
    	).andExpect(status().isOk()).andReturn();
    }
    
    @Test
    public void test_admin_postMonthlySelection() throws Exception{

        MvcResult result = mockMvc.perform(post("/vin/admin/monthly_selection")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("type", "AR")
                        .param("selection_month", "Jan")
                        .param("wines", "")
        ).andReturn();
    }
}