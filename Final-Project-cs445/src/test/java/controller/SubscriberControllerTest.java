package controller;
import application.Application;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
@WebAppConfiguration
public class SubscriberControllerTest {

    @Autowired
    private WebApplicationContext webAppConfiguration;

    private MockMvc mockMvc;

    @Autowired
    SubscriberController subscriberController;

    @Before
    public void setUp(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webAppConfiguration).build();
    }

    @Test
    public void test_sub_post() throws Exception{

        MvcResult result = mockMvc.perform(post("/vin/sub")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("name", "Alice Admin")
                        .param("email", "test@test.com")
                        .param("phone", "1234567890")
                        .param("address", "123 Easy St Anywhere Anystate 23456")
        ).andExpect(status().isCreated()).andReturn();
    }
    
    @Test
    public void test_sub_getByID() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(get("/vin/sub/" + idNum)
                .contentType(MediaType.APPLICATION_JSON)
    	).andReturn();
    }
    
    @Test
    public void test_sub_getShipments() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(get("/vin/sub/" + idNum + "/shipments")
                .contentType(MediaType.APPLICATION_JSON)
    	).andReturn();
    }
    
    @Test
    public void test_sub_getShipmentByID() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(get("/vin/sub/" + idNum + "/shipments/" + idNum)
                .contentType(MediaType.APPLICATION_JSON)
    	).andExpect(status().isNotFound()).andReturn();
    }
    
    @Test
    public void test_sub_getNotes() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(get("/vin/sub/" + idNum + "/shipments/" + idNum + "/notes")
                .contentType(MediaType.APPLICATION_JSON)
    	).andExpect(status().isNotFound()).andReturn();
    }
    
    @Test
    public void test_sub_postNote() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(post("/vin/sub/" + idNum + "/shipments/" + idNum + "/notes")
                .contentType(MediaType.APPLICATION_JSON)
                .param("content", "I really like this wine.")
    	).andExpect(status().isNotFound()).andReturn();
    }
    
    @Test
    public void test_sub_getNote() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(get("/vin/sub/" + idNum + "/shipments/" + idNum + "/notes/" + idNum)
                .contentType(MediaType.APPLICATION_JSON)
    	).andExpect(status().isNotFound()).andReturn();
    }
    
    @Test
    public void test_sub_deleteNote() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(delete("/vin/sub/" + idNum + "/shipments/" + idNum + "/notes/" + idNum)
                .contentType(MediaType.APPLICATION_JSON)
    	).andExpect(status().isNotFound()).andReturn();
    }
    
    @Test
    public void test_sub_getWines() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(get("/vin/sub/" + idNum + "/wines")
                .contentType(MediaType.APPLICATION_JSON)
    	).andReturn();
    }
    
    @Test
    public void test_sub_getWine() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(get("/vin/sub/" + idNum + "/wines/" + idNum)
                .contentType(MediaType.APPLICATION_JSON)
    	).andExpect(status().isNotFound()).andReturn();
    }
    
    @Test
    public void test_sub_getWineNotes() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(get("/vin/sub/" + idNum + "/wines/" + idNum + "/notes")
                .contentType(MediaType.APPLICATION_JSON)
    	).andReturn();
    }
    
    @Test
    public void test_sub_postWineNote() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(post("/vin/sub/" + idNum + "/wines/" + idNum + "/notes")
                .contentType(MediaType.APPLICATION_JSON)
                .param("content", "Very nice wine.")
    	).andExpect(status().isNotFound()).andReturn();
    }
    
    @Test
    public void test_sub_getWineNote() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(get("/vin/sub/" + idNum + "/wines/" + idNum + "/notes/" + idNum)
                .contentType(MediaType.APPLICATION_JSON)
    	).andExpect(status().isNotFound()).andReturn();
    }
    
    @Test
    public void test_sub_deleteWineNote() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(delete("/vin/sub/" + idNum + "/wines/" + idNum + "/notes/" + idNum)
                .contentType(MediaType.APPLICATION_JSON)
    	).andExpect(status().isNotFound()).andReturn();
    }
    
    @Test
    public void test_sub_getWineRating() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(get("/vin/sub/" + idNum + "/wines/" + idNum + "/rating")
                .contentType(MediaType.APPLICATION_JSON)
                .param("rating", "7")
    	).andExpect(status().isNotFound()).andReturn();
    }
    
    @Test
    public void test_sub_getDelivery() throws Exception{
    	MvcResult result = mockMvc.perform(post("/vin/sub")
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", "Alice Admin")
                .param("email", "test@test.com")
                .param("phone", "1234567890")
                .param("address", "123 Easy St Anywhere Anystate 23456")).andExpect(status().isCreated()).andReturn();
    	
    	String IDString = result.getResponse().getContentAsString();
    	JSONObject id = new JSONObject(IDString);
    	int idNum = id.getInt("id");
    	
    	result = mockMvc.perform(get("/vin/sub/" + idNum + "/delivery")
                .contentType(MediaType.APPLICATION_JSON)
    	).andReturn();
    }
}