Upon booting into a fresh installation of Ubuntu 14.04, the following steps can be taken to run the project:

Run the following commands to set up the environment:
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
sudo apt-get install oracle-java8-set-default
sudo apt-get install maven
sudo apt-get install git

Next, clone the git repository:
git clone https://bjones17@bitbucket.org/bjones17/cs445-spring-2015.git

Now run
cd cs445-spring-2015/Final-Project-cs445

Now use Maven to build and test, but be sure to run with "sudo":

sudo mvn clean
sudo mvn package

Create a test coverage file:
sudo mvn cobertura:cobertura

The test coverage file can be found at [working directory]/target/site/cobertura/index.html

Run the application after building with the command
java -jar final-project-1.0.jar